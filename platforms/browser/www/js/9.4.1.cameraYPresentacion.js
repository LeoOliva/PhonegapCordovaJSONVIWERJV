/* 
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

/**
 * 
 * Phonegap/Cordova dispone de un plugin
 * llamado “Camera” el cual provee acceso
 * a la cámara del dispositivo para capturar
 * una foto o accede a la galería de imágenes
 * todo esto a través del objeto global
 * navigator.camera.
 * 
 * 
 * El metodo del objeto navigator.camera usado
 * para obtener alguna imagen es getPicture el 
 * cual recibe dos funciones callback, una en 
 * caso de éxito y otra en cado de error, y un 
 * arreglo de opciones el cual se utiliza para 
 * modificar algunos parametros utilizados al 
 * obtener la imagen. 
 * 
 *  En objeto opcionesCamara:

 *   -La opción quality se refiere a la calidad 
 *      de la imagen que se obtendrá, en un rango de 0 a 100.
 *   -La opción destinationType determina el formato 
 *      del valor de retorno, definido en el objeto 
 *      navigator.camera.DestinationType
 *      
 *      Camera.DestinationType = {
 *      DATA_URL : 0,   //Return image as base64-encoded string
 *      FILE_URI : 1,   //Return image file URI
 *      NATIVE_URI : 2  //Return image native URI 
 *                      (e.g., assets-library:// on IOS 
 *                      or content:// on android)
 *      };
 *      
 *  La opción sourceType determina la fuente de la imagen, 
 *  definida en el objeto navigator.camera.PictureSourceType
 *  
 *  Camera.PictureSourceType = {
 *      PHOTOLIBRARY : 0,
 *      CAMERA : 1,
 *      SAVEDPHOTOALBUM : 2
 *  }
 * 
 *  En funciones callback:
 * -El valor que recibe la función callback en caso exitoso 
 *  está determinado por el valor especificado en la opción destinationType
 * -El valor que recibe la función callback en caso de error 
 *  es un mensaje provisto por el código nativo del dispositivo
 * 
 */


$('#imagenCamara').click(function() {
    console.log("Click en imagenCamara");
        var opcionesCamara= {
             quality             : 90,
             destinationType     : Camera.DestinationType.DATA_URL,
             sourceType          : Camera.PictureSourceType.Camera
         };
         navigator.camera.getPicture(exitoEnFoto, //onSuccess callback
                                    errorEnFoto,  //onError callback
                                    opcionesCamara); //personalizacion de ajustes de camara
    }    
);

function exitoEnFoto(imageData){
    var espacioFoto = document.getElementById("foto");
    espacioFoto.src = "data:image/jpeg;base64," + imageData;
};
        
function errorEnFoto(message) {
    alert (" Error en foto = "+ message);
};


