/* 
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */


/**
 * 
 * El plugin “Geolocation” proporciona acceso a la información 
 * de ubicación del dispositivo (latitud, longitud, etc), obteniendo 
 * datos del sensor GPS (Sistema de posicionamiento global) o 
 * de redes celulares cercanas.
 * 
 * Tenemos 3 métodos disponibles relacionados con la geolocalización. 
 * Estos métodos son parte del objeto global navigator.geolocation. 
 * Este objeto tiene 3 métodos:
 * 
 *  -getCurrentPosition: Obtiene la ubicación actual.
 *  -watchPosition: espera una nueva ubicación del dispositivo y 
 *      lo devuelve como parámetro en la función callback.
 *  -clearWatch: Detiene la espera de la nueva ubicación iniciada 
 *      por el método watchPosition.
 * 
 * Para información detallada de la API de Geolocalización, revise:
 *
 *   http://dev.w3.org/geo/api/spec-source.html
 *   
 *   
 *   
 * En opcionesGeo:
 *
 *  -La llave timeout especifica el tiempo máximo de espera en milisegundos.
 *  -La llave enableHighAccuracy indica si se necesita el mejor resultado 
 *      posible. Por default el dispositivo intenta obtener la ubicación actual 
 *      en base a redes celulares cercanas.
 *      
 *  En la función watchPosition: 
 * 
 *  -El parámetro geoLocalizacionExito indica la función callback que se llamará cuando se obtenga una nueva posición.
 *  -El parámetro geoLocalizacionError indica la función callback que se llamara en caso de algún error.
 *  -El parámetro opcionesGeo especifica los parámetros opcionales para la obtención de la ubicación.
 *  
 *  En la función geoLocalizacionExito el parámetro obtenido :
 *
 *  -posicion.coords : un paquete de coordenadas geográficas.
 *  -posicion.timestamp : la marca de tiempo de cuando obtuvo la nueva ubicación.
 * 
 */


var watchPositionID = null;
var aquiEstoy = null;
var getPosition = false;

$(document).on('pagecontainershow', function(event,ui){
    var paginaAMostrar = ui.toPage.prop("id");
    console.log("Se cargo la pagína geolocalización");
    if(paginaAMostrar == 'geolocalización'){
       alert("GEOLOCALIZACION");
        buscaPosicion();
    } 
});


function buscaPosicion(){
    
    aquiEstoy = document.getElementById('localizacion');
    aquiEstoy.innerHTML = 'Obteniendo ubicacion...<br/> Habilita su GPS..<br/>';
        
    var opcionesGeo = { timeout: 50000, enableHighAccuracy: true };


        console.log("Buscando...");
        watchPositionID = navigator.geolocation.watchPosition(
                    geoLocalizacionExito,
                    geoLocalizacionError,
                    opcionesGeo
                );
}

function geoLocalizacionExito(position_){
    position = position_;
    aquiEstoy.innerHTML = 'Latitud: ' + position_.coords.latitude + '<br/>' +
                          'Longitud: ' + position_.coords.longitude + '<br/>' +
                          'Altitud: ' + position_.coords.altitude + '<br/>' +
                          'Exactitud: ' + position_.coords.accuracy + '<br/>' +
                          'Exactitud en altitud: ' + position_.coords.altitudeAccuracy + '<br/>'+
                          'Direccion: ' + position_.coords.heading + '<br/>' +
                          'Velocidad: ' + position_.coords.speed + '<br/>'+
                          'Marca de tiempo: ' + position_.timestamp;
   if(!getPosition)
   presentaGoogleMapsEnUbicacion(position_);
   getPosition = true;
}

function geoLocalizacionError(error){
    alert("Error en Geolocalización: " + error.message);
}
