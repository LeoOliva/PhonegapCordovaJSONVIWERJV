/* 
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

/**
 * Podemos usar en cualquier momento la vibración del 
 * dispositivo, esto es posible gracias a uno de los 
 * plugins del núcleo de PhoneGap/Cordova llamado “Vibration”.
 * 
 *  Este plugin define el método vibrate en el objeto global navigator.
 *  El objeto global navigator solo está disponible hasta después del 
 *  evento “deviceready”.
 *
 *   En el método navigator.vibrate :
 *
 *   -El parámetro especifica el número de milisegundos que vibrara el dispositivo.
 * 
 * 
 * @param {int} time Tiempo estimado para activar el evento
 * 
 */

function vibracionDispositivo(time){
    navigator.vibrate(time);
}


$("#vibrar").click(function(){
    vibracionDispositivo(3000);
});
