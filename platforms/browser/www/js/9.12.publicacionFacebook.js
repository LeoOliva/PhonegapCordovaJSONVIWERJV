/* 
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

$("#publicarEnFacebok").click(function (){
    facebookConnectPlugin.login(["public_profile"],facebookLoginExito,facebookLoginError);
});

function facebookLoginError(error) {
    alert("Error Login: " +error.errorMessage);
};

function facebookLoginExito(userData) {
    if (userData.status == 'connected') {
        facebookConnectPlugin.getLoginStatus(facebookLoginStatus);
    } else{
        alert("No se ha iniciado sesion en Facebook.");
    }
};

function facebookLoginStatus(statusActual) {
    if (statusActual.status == 'connected') {
        var options = { method:"feed" };
        facebookConnectPlugin.showDialog(options, publicacionExito,publicacionError);
    }
    else{
        alert("No se ha autorizado a la aplicacion en Facebook.");
    }
};


function publicacionExito(result){
    alert("Publicado con ID : " + result.post_id);
};

function publicacionError(error){
    alert("Error publicacion : " + error);
};