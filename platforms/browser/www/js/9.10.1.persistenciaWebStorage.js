/* 
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

/**
 * 
 * Una de las nuevas capacidades de HTML5, es la posibilidad de 
 * guardar información en el navegador del usuario, de lado del 
 * cliente si hablamos del esquema “cliente-servidor”. Además de 
 * superar los problemas de las cookies, la mayoría de los navegadores 
 * soportan al menos 5 MB de WebStorage. 
 * 
 * El almacenamiento en WebStorage es en formato de pares 
 * llave-valor (key/value) en la cual cualquier valor que 
 * se quiera guardar se identificara con una llave, misma 
 * que servirá para recuperarla.
 * 
 * Hay dos tipos de WebStorage, diferenciados solo por la duración 
 * que la informacion estara disponible, los cuales involucran 
 * trabajar con dos objetos:
 * 
 *  -El objeto Local Storage (window.localStorage): Los datos permanecen 
 *      infinitamente disponibles.
 *   -El objeto Session Storage (window.sessionStorage): Los datos sólo estarán 
 *       disponibles durante la sesión de navegación y cuando esta se 
 *       cierre, los datos desaparecen
 *       
 *  NOTA: Al usar WebStorage, los datos siempre se almacenarán como texto. 
 *      Se deben usar funciones de conversión para obtener datos en algún 
 *      otro tipo (números, fechas, etc). 
 * 
 */

var llaveAcceso = "numeroAccesoApp";

$(document).on('pagecontainerbeforeshow',function (event,ui){
    var paginaAMostrar = ui.toPage.prop("id");
    if(paginaAMostrar == "webStorage"){
        console.log("webStorage encontrado");
        //Se verifica si tiene soporte para WebStorage
        if(typeof(window.localStorage) !== 'undefined'){
           var numAccesoAnterior = 
                   window.localStorage.getItem(llaveAcceso); // Recupera el valor de la llave
           var numAccesoActual;
           if(!numAccesoAnterior){
               numAccesoActual = 1;
           } else{
               numAccesoActual = parseInt(numAccesoAnterior) + 1;
           }
           //Guarda el valor asociandolo con la llave
           //El primer parámetro especifica la llave con la que se guardará el valor
           //El segundo parámetro especifica que es lo que se va a guardar
           window.localStorage.setItem(llaveAcceso,numAccesoActual);
           console.log("numAccesoActual:" +numAccesoActual);
           $("#numeroAcceso").html(numAccesoActual);
        }else{
            alert("No hay soporte para WebStorage");
        }
    }
    
    
});

