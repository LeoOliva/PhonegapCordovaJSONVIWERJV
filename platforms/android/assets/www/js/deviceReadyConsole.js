/* 
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
document.addEventListener("deviceready",onDeviceReady,false);


function onDeviceReady(){
    console.log("Escribiendo a log. Evento deviceready!!! ...");
    console.log("Creando vigilante para evento tap en boton");
    $("#btnDialogoNativo").on("tap",onTapEventNative);
    $("#btnDialogoGenerico").on("tap",onTapEventGeneric);
    $("#btnDialogoCargaEspera").on("tap",onTapEventLoadWait);
    ////////////////////////////////////////////////
    $("#irPerfil").on("tap",onTapGoToProfileJS);
    
}
/////////////////////////////////

function onTapGoToProfileJS(){
    console.log("Ir a perfil desde Javascript");
    showNotificationLoadWait("Ir a perfil desde Javascript","b","");
    setTimeout(hideNotificacionLoadWait,2500);
    reRoute("#perfil");
    
}


/////////////////////////////

function onTapEventLoadWait(){
    showNotificationLoadWait(
            "Se ha hecho click sobre el boton de Carga/espera",
            "b",
            ""
    );
    setTimeout(hideNotificacionLoadWait,2500);
}



/////////////////////////////

function onTapEventGeneric(){
    notificationGeneric("Se ha generado un mensaje generico");
}



//////////////////////////////

function onTapEventNative(){
    notificationNative("Evento Tap!!!!","Se a detectado el evento tap","OK!");
}

