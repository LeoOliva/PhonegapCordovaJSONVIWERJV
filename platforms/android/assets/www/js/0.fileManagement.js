function readFile_(evt,output){
    switch(device.platform){
        case "browser":
            $.getScript('js/0.browserFileManagement.js',function(data, textStatus, jqxhr) {
                //console.log(data); //data returned
                //console.log(textStatus); //success
                //console.log(jqxhr.status); //200
                //console.log('Load was performed.');
                browserReadFile(evt,output);
             });
            break;
        case "Android":
            $.getScript('/android_asset/www/js/0.androidFileManagement.js',function(data,textStatus,jqxhr){
                //console.log(data); //data returned
                //console.log(textStatus); //success
                //console.log(jqxhr.status); //200
                //console.log('Load was performed.');
                androidReadFilePersistent(evt,output);
                androidCreateTemporaryFile();
            });
            break;
        default:
            var_dump("Platform does not supported");
    }
    }