/* 
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

/**
 *
 * El plugin “Acceleromatter” provee acceso a el acelerómetro 
 * del dispositivo. Este dispositivo interno es un sensor de 
 * movimiento que detecta cambios o deltas en movimientos relativos 
 * a la orientación actual del dispositivo en 3 dimensiones (x, y, z). 
 *   
 *   
 *   
 *  El acceso al dispositivo interno se hace a través del objeto global 
 *      navigator.accelerometer. Este objeto tiene 3 métodos:
 *
 *   -getCurrentAcceleration: Obtiene los valores actuales del acelerómetro.
 *   -watchAcceleration: Obtiene los valores del acelerómetro en intervalos 
 *      regulares y los devuelve como parámetro en la función callback.
 *   -clearWatch: Detiene la lectura de los datos del acelerómetro iniciados 
 *      por el método watchAcceleration.
 *      
 *      
 *      
 *      
 *  En opcionesLecturaAccel :
 *
 *   -La llave frequency especifica el intervalo de lectura en que se leerá 
 *      la información del acelerómetro.
 *      
 *  En la función intervaloLecturaExito el parámetro obtenido :
 *
 *   -acceleration.x contiene la cantidad de aceleración en el eje x.
 *   -acceleration.y contiene la cantidad de aceleración en el eje y.
 *   -acceleration.z contiene la cantidad de aceleración en el eje z
 *   -acceleration.timestamp contiene la marca de tiempo en milisegundos.
 *      
 */

function obtenerInformacionAcelerometro(){
    var opcionesLecturaAcelerometro = {frequency:3000}; //Actualizar cada 3 segundos
    navigator.accelerometer.watchAcceleration(
                intervaloLecturaExito,
                intervloLecturaError,
                opcionesLecturaAcelerometro
            );
}

function intervaloLecturaExito(acceleration){
    console.log("Exito en la lectura del acelerometro");
    $("#infoDispositivoAcelerometro").html(
            "Acceleration X: " + acceleration.x + "<br/>" +
            "Acceleration Y: " + acceleration.y + "<br/>" +
            "Acceleration Z: " + acceleration.z + "<br/>" +
            "Marca de tiemp: " + acceleration.timestamp + "<br/>"
            );
}
function intervloLecturaError(){
    console.log("Error al leer el  Acelerometros");
}
