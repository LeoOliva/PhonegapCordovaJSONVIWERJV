/* 
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

/**
 * 
 * Phonegap/Cordova ofrece también un plugin 
 * llamado “Media” que permite la posibilidad 
 * de reproducir y grabar archivos de audio en el 
 * dispositivo.
 * 
 * Para usar esta funcionalidad se debe agregar un plugin 
 * llamado “org.apache.cordova.media” el cual necesita
 * a su vez, el plugin “File API” para tener acceso a 
 * directorios de archivos locales (en el dispositivo). 
 * 
 * 
 * La forma más básica de usar este plugin requiere crear 
 * un objeto JavaScript llamado “Media” el cual necesita,
 * al menos, una URI especificando el archivo de audio 
 * que se reproducirá. 
 * 
 * 
 * Una vez que tenemos creado el objeto “Media” tenemos a nuestra 
 * disposición métodos para controlar la reproducción y/o grabación 
 * de audio. Algunos de los cuales son los siguientes:
 * 
 *  -media.play : Inicia o reanuda la reproducción de audio.
 *  -media.pause : Pausa la reproducción de audio.
 *  -media.stop : Detiene la reproducción de audio.
 */



var miAudio;

$("#audioPlay").click(function(){
    if(miAudio){
        miAudio.stop();
        miAudio = null;
        $("#audioPlay").html("Audio favorito - Play");
    } else{
        //miAudio = new Media("http://audio.ibeat.org/content/p1rj1s/p1rj1s_-_rockGuitar.mp3");
        miAudio = new Media ("/android_asset/www/audio/p1rj1s_-_rockGuitar.mp3");
        //Número de repeticiones
        miAudio.play({numberOfLoops:1});
        $("#audioPlay").html("Audio favorito - Stop");
    }
    
});
$("#audioPause").click(function(){
     if(miAudio){
         miAudio.pause();
     }
    
});

