 /**
 * http://rickluna.com/wp/2013/12/browsing-filesystems-in-phonegap/
 * http://rickluna.com/wp/2014/01/accessing-external-storage-in-android-phonegap-3-3/
 *
 * 
 *  https://github.com/apache/cordova-plugin-file
 *  http://stackoverflow.com/questions/22868089/how-to-access-external-storage-with-cordova-file-file-system-roots-plugins
 *  https://github.com/xjxxjx1017/cordova-phonegap-android-sdcard-full-external-storage-access-library
 *  https://cordova.apache.org/docs/en/latest/reference/cordova-plugin-file/index.html
 *  http://docs.phonegap.com/en/edge/cordova_file_file.md.html#FileEntry
 *  https://github.com/apache/cordova-plugin-file
 *  https://github.com/apache/cordova-plugin-file
 *  https://cordova.apache.org/docs/en/latest/reference/cordova-plugin-device/
 *  
 *    
 * 
 */
/*
 * Uno de los principales problemas que tuve durante el desarrollo fue la especificación
 * de la ruta de acceso, me retornaba la ruta de la APP y no de la SDCARD. Esto se solucionó
 * agregando la siguiente linea al archivo de configuración:
 * 
 * <preference name="AndroidPersistentFileLocation" value="Compatibility" />
 * 
 * al colocar esta
 * <preference name="AndroidPersistentFileLocation" value="Internal" />
 * maneja la ruta interna
 * https://github.com/apache/cordova-plugin-file
 * 
 * https://github.com/apache/cordova-plugin-file#android-file-system-layout
 * 
 */
function androidReadFilePersistent(evt,output){
     window.requestFileSystem(window.LocalFileSystem.PERSISTENT, 0, function(fileSystem){
        fileSystem.root.getFile(getPathFile(evt), {create: false, exclusive: false} , function(fileEntry){
          ///Read FILE
          fileEntry.file(function (file) {
          var reader = new FileReader();

            reader.onloadend = function() {
                console.log(this.result); 
                readFile(fileEntry,setOutputFile,output);
            };
            reader.readAsText(file);

        }, fail);
          
          ///
        }, fail);
     }, fail);
}


function androidCreateTemporaryFile(){
    window.requestFileSystem(window.TEMPORARY, 5 * 1024 * 1024, function (fs) {

    console.log('file system open: ' + fs.name);
    createFile(fs.root, "newTempFile.txt", false);

    }, fail);
}

function getPathFile(fullPath){
    return fullPath.substr(cordova.file.externalRootDirectory.split("file://")[1].length);
}

function fail(error) {
        console.log(error.code);
    }
    

function createFile(dirEntry, fileName, isAppend) {
    // Creates a new file or returns the file if it already exists.
    dirEntry.getFile(fileName, {create: true, exclusive: false}, function(fileEntry) {

        writeFile(fileEntry, null, isAppend);

    }, fail);

}


function writeFile(fileEntry, dataObj) {
    // Create a FileWriter object for our FileEntry (log.txt).
    fileEntry.createWriter(function (fileWriter) {

        fileWriter.onwriteend = function() {
            console.log("Successful file read...");
            readFile(fileEntry,null,null);
        };

        fileWriter.onerror = function (e) {
            console.log("Failed file read: " + e.toString());
        };

        // If data object is not passed in,
        // create a new Blob instead.
        if (!dataObj) {
            dataObj = new Blob(['some file data'], { type: 'text/plain' });
        }

        fileWriter.write(dataObj);
    });
}




function readFile(fileEntry,callback,output) {

    fileEntry.file(function (file) {
        var reader = new FileReader();

        reader.onloadend = function() {
            console.log("Successful file read: " + this.result);
            if((callback) && (typeof callback === 'function')){
                //callback  is a function
                callback(this.result,output);
            }
            //displayFileData(fileEntry.fullPath + ": " + this.result);
        };

        reader.readAsText(file);

    }, fail);
}

function setOutputFile(content,output){
    if($.trim(content).length > 0){
        output.val(content);
        output.trigger("change");
    }else{
        alert("The file is empty");
    }
}