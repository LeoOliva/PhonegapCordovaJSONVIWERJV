/* 
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

/**
 * Utilizaremos un plugin que no se considera de los 
 * plugins de núcleo de PhoneGap/Cordova para tener 
 * la capacidad de que alguna app lanze notificaciones.
 * El plugin se llama “Local-Notification” y nos permite 
 * lanzar notificaciones en la barra de estado. De acuerdo 
 * al README del plugin, es compatible, a la fecha de escribir 
 * este documento, con : 
 *
 *  -iOS(including iOS 8)
 *  -Android (SDK >=7)
 *  -Windows 8.1 (added with v0.8.2)
 *  -Windows Phone 8.1 (added with v0.8.2)
 * 
 * Se localiza en la siguiente dirección git:
 * 
 * de.appplant.cordova.plugin.local-notification=https://github.com/sebasi/cordova-plugin-local-notifications.git
 * 
 * Lo agregamos en el archivo plugins.properties junto con el plugin 
 * de la librería de soporte v4 de android 
 * (requerida para cuestiones de compatibilidad en el SO Android):
 * 
 * cordova-plugin-android-support-v4-jar=https://github.com/pbakondy/cordova-plugin-android-support-v4-jar.git
 * 
 * 
 * Mediante el método cordova.plugins.notification.local.hasPermission() 
 * se puede obtener si la aplicación está autorizada para lanzar 
 * notificaciones en iOS 8 y superiores. 
 * 
 *  El método cordova.plugins.notification.local.schedule() 
 *  nos permite lanzar una notificación. El parámetro que recibe 
 *  consta de varios parámetros opcionales. Algunos de los cuales son:
 *
 *   -id: un identificador único de la notificación.
 *   -text: El texto que contendrá la notificación.
 *   
 *   -----------------------------------------------------------------
 *   Ejemplo del README del plugin
 *   
 *   cordova.plugins.notification.local.schedule({
 *       id: 1,
 *       title: "Production Jour fixe",
 *       text: "Duration 1h",
 *       firstAt: monday_9_am,
 *       every: "week",
 *       sound: "file://sounds/reminder.mp3",
 *       icon: "http://icons.com/?cal_id=1",
 *       data: { meetingId:"123#fg8" }
 *   });
 *
 *   cordova.plugins.notification.local.on("click", function (notification) {
 *       joinMeeting(notification.data.meetingId);
 *   });
 * 
 */


$("#notificacionPermiso").click(function (){
    cordova.plugins.notification.local.hasPermission(function(granted){
        alert("Hay permiso? = " + granted);
        
    });
});

$("#notificacion").click(function(){
    
    cordova.plugins.notification.local.schedule({
    id: 1,
    title: "Production Jour fixe",
    text: "Duration 1h",
    //firstAt: monday_9_am,
    //every: "week",
    sound: "/android_asset/www/audio/p1rj1s_-_rockGuitar.mp3",
    icon: "http://icons.iconarchive.com/icons/designbolts/free-multimedia/1024/iMac-icon.png",
    //data: { meetingId:"123#fg8" }
});
/*
    cordova.plugins.notification.local.schedule({
        id : 1,
        text : "Prueba de notificacion local"
    });
    */
    
});

cordova.plugins.notification.local.on("click", function (notification) {
    console.log("Click en notificacion");
    alert(notification.text);
});