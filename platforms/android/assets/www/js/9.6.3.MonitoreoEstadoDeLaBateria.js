/* 
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

/**
 *
 * El plugin “Battery Events” agrega 3 nuevos eventos 
 *  relativos a cambios de estado de la batería del dispositivo:
 *  
 *   -batterystatus: Evento que se lanza cuando el porcentaje 
 *      de carga de la batería cambia por al menos 1% y 
 *      cuando el dispositivo se conecta o se desconecta.
 *   -batterycritical: Evento que se lanza cuando el porcentaje 
 *      de carga de la batería ha llegado a un umbral crítico, el 
 *      cual depende del dispositivo.
 *   -batterylow: Evento que se lanza cuando el porcentaje de 
 *      carga de la batería ha llegado a un umbral de batería 
 *      baja, el cual depende del dispositivo. 
 *
 *      -----------------------------------------------------------
 *  En el objeto info que se recibe en la función callback:
 *
 *   -La propiedad level contiene el porcentaje de carga de la batería (0-100).
 *   -La propiedad isPlugged indica si el dispositivo está cargando o no      
 * 
 */

function obtenerInformacionBateria(){
    window.addEventListener("batterystatus",onBatteryStatusChange,false);
}
function onBatteryStatusChange(info){
    $("#infoDispositivoBateria").html(
            "Cargando: " + info.isPlugged + "<br/>"+
            "Nivel de carga: " + info.level
            
            );
}