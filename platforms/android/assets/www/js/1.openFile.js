/**
 * 
 * When the page is loaded, the choose file dialog is shown
 * 
 * I use MFileChooser plugin
 * from:
 * https://www.npmjs.com/package/cordova-plugin-mfilechooser
 * 
 * nl.fellow-it.mfilechooser=https://github.com/Langstra/MFileChooser.git
 * 
 * https://github.com/Langstra/MFileChooser
 */

$(document).on("pagecontainerbeforeshow",function(event,ui){
    var idPageToShow = ui.toPage.prop("id");
    if(idPageToShow == "openFile"){
        $("#openFileDialog").parent().hide();
        $("#openFile_edit").hide();
        openFileDialog();
    }
    
});

/**
 * 
 * Open the file dialog for each platform
 * 
 */
function openFileDialog(){ 
    
    if(device.platform != "browser"){
            window.plugins.mfilechooser.open(['.json'], function (uri) {

              //alert(uri);
              openFile(uri);

            }, function (error) {

                alert(error);

            });
         }else{          
               $('#openFileDialog').click();
         }
}

/**
 * When the open button is clicked
 */

$("#openFile_Open").click(function(){
    openFileDialog();    
});


/**
 *
 * When changing the  file  
 */
 $('#openFileDialog').change(function(evt){
     openFile(evt);
 });
 
 
 /**
  * 
  * Hide or show the edit button
  */
 $("#openFile_area").bind("input propertychange change",function(){
    if($.trim($("#openFile_area").val()).length > 1){
         //Is not empty
         alert("IS NOT EMPTY");
         $("#openFile_edit").show();
     }else{
         alert("IS EMPRTY");
         $("#openFile_edit").hide();
     }
 });

/**
 * Reading the file 
 */
function openFile(evt){
    readFile_(evt,$("#openFile_area"));
}
