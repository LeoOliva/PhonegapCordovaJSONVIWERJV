/* 
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

/**
 * El plugin “Compass” provee acceso a la brújula del dispositivo. 
 * Este dispositivo interno es un sensor que detecta la dirección 
 * a la que el dispositivo está apuntando, típicamente desde la 
 * parte superior del dispositivo. La dirección la mide en grados 
 * de 0 a 359.99 donde 0 es el norte. 
 * 
 * 
 *  El acceso al dispositivo interno se hace a través del objeto 
 *  global navigator.compass. Este objeto tiene 3 métodos:
 *
 *  -getCurrentHeading: Obtiene la dirección actual de la brújula.
 *  -watchHeading: Obtiene la dirección del dispositivo en intervalos 
 *      regulares y lo devuelve como parámetro en la función callback.
 *  -clearWatch: Detiene la lectura de los datos de la brújula 
 *      iniciados por el método watchHeading.
 * 
 * 
 *  En opcionesLecturaBrujula:
 *  -La llave frequency especifica el intervalo de lectura en que se leerá la información del acelerómetro.
 *
 *
 * En la función intervaloLecturaBrujulaExito el parámetro obtenido:
 *    -heading.magneticHeading : La dirección en grados en un momento del tiempo.
 * 
 */


function obtenerInformacionBrujula(){
    var opcionesLecturaBrujula = { frequency : 1000 };
    navigator.compass.watchHeading(
            intervaloLecturaBrujulaExito,
            intervaloLecturaBrujulaError,
            opcionesLecturaBrujula
            );
}

function intervaloLecturaBrujulaExito(heading){
    console.log("Direccion: " + heading.magneticHeading);
    $("#infoDispositivoBrujula").html("Direccion: " + heading.magneticHeading);
}

function intervaloLecturaBrujulaError(compassError){
    alert("Error al leer la brujula " + compassError.code);
}