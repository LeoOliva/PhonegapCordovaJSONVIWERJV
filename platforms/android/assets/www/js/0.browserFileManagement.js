function browserReadFile(evt,output){
     //Retrieve the first (and only!) File from the FileList object
    var f = evt.target.files[0]; 
    if (f) {
      var r = new FileReader();
      r.onload = function(e) { 
	var contents = e.target.result;
        if($.trim(contents).length > 0){
        output.val(contents);
        output.trigger("change");
        }else{
            alert("The file is empty");
        }
        /*
        alert( "Got the file.n" 
              +"name: " + f.name + "\n"
              +"type: " + f.type + "\n"
              +"size: " + f.size + " bytes\n"
              + "starts with: " + contents.substr(1, contents.indexOf("n"))
        );  
        */
      };
      //Is a asynchronous call
      r.readAsText(f);
    } else { 
      alert("Failed to load file");
    }
    
}
