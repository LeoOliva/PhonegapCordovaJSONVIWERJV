/* 
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

/**
 * 
 * El plugin “Device” nos permite obtener detalles del
 * hardware y del software del dispositivo; a través 
 * del objeto global device. 
 * 
 * Aquí usamos un evento “show” del widget pagecontainer 
 * para ejecutar nuestro código cuando la página con el ID 
 * “informacionDispositivo” esté mostrada totalmente.
 * 
 */

/**
 *  En el objeto global device:
 *   -La propiedad model contiene el modelo del dispositivo.
 *   -La propiedad cordova contiene la versión de cordova que 
 *      está corriendo en el dispositivo.
 *   -La propiedad platform contiene el nombre de la plataforma 
 *      en la que se está ejecutando la app.
 *   -La propiedad uuid contiene el identificador universalmente 
 *      único del dispositivo.
 *   -La propiedad version contiene la versión del 
 *      sistema operativo del dispositivo.
 * 
 * 
 */


function obtenerInfomacionDispositivo(){
    var string = "Modelo: " + device.model + "<br />" +
            "Version de Cordova: " + device.cordova + "<br/>"+
            "plataforma: " + device.platform + "<br/>" +
            "UUID (Identificador único universal o universally unique identifier): " + device.uuid + "<br/>"+ 
            "Version OS:" + device.version + "<br/>";
   // console.log(string);
    $("#infoDispositivo").html(string);
}
