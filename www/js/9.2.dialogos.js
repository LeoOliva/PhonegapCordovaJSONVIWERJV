/* 
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
//////////////////////////////////////
//////////////////////////////////////
/**
 * 
 * Si se desea usar un diálogo con más personalización 
 * podemos hacer uso de un plugin de PhoneGap/Cordova 
 * llamado “Dialogs” el cual provee acceso a algunos
 *  elementos de la interfaz de diálogos nativos a través 
 *  del objeto global navigator.notification. El objeto 
 *  global navigator solo está disponible hasta después 
 *  del evento “deviceready”. 
 * 
 */
function notificationNative(mensaje,titulo,tituloBoton){
    navigator.notification.alert(
            mensaje,    //Mensaje a mostrar
            alertDismissed,//función callback
            titulo, //título
            tituloBoton     //texto del  boton       
            );
}

function alertDismissed(){
    console.log("Function alertDismissed");
}



//////////////////////////////////////
////////////////////////////////////////

function notificationGeneric(msj){
    alert(msj);
}

///////////////////////////////
//////////////////////////////////

/**
 * jQuery Mobile nos ofrece el widget Loader 
 * que nos permite mostrar un diálogo de
 *  carga o espera. Ideal para mostrarse mientras
 *   se hace una operación de larga duración 
 *   o realizar un contenido vía Ajax. 
 */

function showNotificationLoadWait(text,theme,html){
   $.mobile.loading(
            "show",
            {
                text:text,  //text:"Iniciando sesión",
                textVisible:true, //textVisible:true,
                theme:theme, //theme:"b",
                html:html   // html:""

            }
            
            );   
}
 function hideNotificacionLoadWait(){
     $.mobile.loading("hide");
 }

//////////////////////////////////
//////////////////////////////////////