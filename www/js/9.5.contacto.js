 /* 
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

/**
 * 
 * El plugin “Contacts” nos permite acceder a 
 * la base de datos de contactos del dispositivo. 
 * A través del uso de este plugin podemos crear 
 * y consultar contactos. 
 * 
 * La función que usaremos sera el metodo contacts.find 
 * la cual nos permite hacer consultas asíncronas a la 
 * base de datos de contactos del dispositivo, regresando 
 * un arreglo de objetos Contact.
 * 
 * navigator.contacts.find(contactFields,contactSuccess,contactError,contactFindOptions);
 * 
 *  Este método recibe los siguientes parámetros:
 *
 *  -El parámetro contactFields especifica los campos que se desean consultar 
 *  y a su vez solo esos campos se incluirán en el objeto Contact que se devuelve 
 *  a la función callback de éxito contactSuccess.
 *      --El objeto Contact puede contener varios elementos o campos que describen 
 *          a un contacto. Algunos de los cuales son:
 *           ---displayName : El nombre para mostrar de un contacto.
 *           ---phoneNumbers :  Arreglo de los numeros de telefono del contacto.
 *           ---emails : Arreglo de los correos del contacto.
 * 
 * -------------------------------------------------------------------------
 * -El parámetro contactSuccess es la función callback que se invocará 
 *  cuando la consulta termine. Contendrá un arreglo de objetos Contact 
 *  conteniendo los campos especificados por contactFields.
 * -El parámetro contactError es la función callback que se invocará 
 *  cuando ocurra algún error (opcional).
 * -El parámetro contactFindOptions especifica las opciones búsqueda 
 *  para filtrar los contactos(opcional).
 * -------------------------------------------------------------------------
 * -El método append de la lista desordenada (<ul>) agrega un elemento de lista (<li>).
 * -El método refresh del widget Listview de jQuery Mobile actualiza la apariencia 
 * visual para los elementos agregados.
 * 
 * 
 * 
 * 
 * http://docs.phonegap.com/en/edge/cordova_contacts_contacts.md.html#Contact
 * 
 * 
 * 
function onSuccess(contacts) {
    alert('Found ' + contacts.length + ' contacts.');
};

function onError(contactError) {
    alert('onError!');
};

// find all contacts with 'Bob' in any name field
var options      = new ContactFindOptions();
options.filter   = "Bob";
options.multiple = true;
var fields       = ["displayName", "name"];
navigator.contacts.find(fields, onSuccess, onError, options);
 
 
 */

$("#consultaContactos").click(function (){
    showNotificationLoadWait("Cargando","b","");
    consultarContactos();
    hideNotificacionLoadWait();
});


function consultarContactos(){
    console.log("Consultando.....");
    var camposABuscar = ["displayName","phoneNumbers"];
    
    navigator.contacts.find(camposABuscar,contactosExito,contactosError);
}

function contactosExito(contactos){
    console.log("Generando lista-...");
    for (var i = 0; i < contactos.length; i++) {
        if(contactos[i].displayName){
                   $("#listaContactos").append("<li>" + contactos[i].displayName +"</li>")
        }
    }
    $("#listaContactos").listview("refresh");
    
}

function contactosError(error){
    alert("Error = " + error);
}

