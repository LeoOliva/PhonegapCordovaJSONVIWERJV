/* 
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */


/**
 *  Ya que sabemos que jQuery Mobile está basado y necesita 
 *  de la librería jQuery para funcionar, usaremos una función 
 *  de jQuery que permite solicitar o intercambiar datos con un 
 *  servidor sin necesidad de recargar o actualizar el documento 
 *  HTML completo, técnica conocida como AJAX.
 *
 * jQuery dispone de muchas funciones para diferentes necesidades 
 * aplicables a esta técnica. Las cuales podemos consultar una 
 * lista de ellas en la siguiente página:
 * 
 * http://www.w3schools.com/jquery/jquery_ref_ajax.asp
 * 
 * Una de estas funciones es $.getJSON()
 * la cual carga datos codificados en formato JSON desde un 
 * servidor usando una petición HTTP GET. 
 * 
 * Las peticiones y el consumo de datos por internet requiere 
 * el uso de un plugin llamado “cordova-plugin-whitelist” 
 * localizado en :
 * 
 * https://github.com/apache/cordova-plugin-whitelist
 * 
 * 
 * Este plugin controla el acceso a dominios externos sobre 
 * los cuales la app no tiene el control. La política de seguridad 
 * cuando no se tiene este plugin, por default, bloquea el acceso a 
 * cualquier dominio. Si queremos que la app tenga acceso a algún 
 * dominio se debe formular una lista blanca o “whitelist” de esos 
 * dominios.
 * 
 * Esta lista blanca o “whitelist” se realiza conforme a la 
 * especificación “Widget Access” de la W3C, la cual establece 
 * que se debe agregar un elemento <access> dentro el archivo 
 * config.xml para cada dominio al cual se quiera tener acceso 
 * desde la app.
 * 
 *  Para saber más de la “W3C Widget Access”, consulte:
 *
 *   http://www.w3.org/TR/widgets-access/
 *   
 *   
 *  En la función $.getJSON :
 *  
 *  -El parámetro urlWS especifica la URL a la que se hará la 
 *      petición.
 *  -En el segundo parámetro se especifican los datos que se 
 *      enviarán al servidor.
 *  -El parámetro respuestaJSON es la función callback la cual 
 *      responderá cuando la petición haya terminado.  
 *   
 * En la función callback respuestaJSON :
 *  
 *  -El parámetro respuestaServer contiene los datos JSON recibidos del servidor.
 *  -En el segundo status contiene el estado final de la petición, algunos de los posibles valores son: 
 *       -success
 *       -error
 *       -timeout
 *       -parsererror
 * 
 */

$("#consultaWebService").click(function(){
    consultaServicioWeb();
    
});

function consultaServicioWeb(){
    //showNotificationLoadWait("Consultando","b","");
    //var urlWS = "http://www.elguille.info/NET/WebServices/HolaMundoWebS.asmx?op=Saludo";
    //var urlWS = "http://omnius.com.mx:8080/OmniusBackEnd/webresources/service/usuarioLog?telefono=3312062309&contrasena=631dc04008e00421214c85f99a6be931";
    var urlWS = "http://omnius.com.mx:8080/OmniusBackEnd/webresources/service/usuarioLog?telefono=3312062309&contrasena=escarlata";
    $.getJSON(urlWS,null,respuestaJSON);
 }
 
function respuestaJSON(respuestaServer,status){
    //hideNotificacionLoadWait();
    if(status == "success"){
        alert(respuestaServer);
        console.log(respuestaServer);
           $('#infoUsuario').html(respuestaServer.usuario.nombre +" "+ respuestaServer.usuario.apellidos + "<br />");
        $('#infoUsuario').append("Examenes : " +respuestaServer.examenLoges.length + "<br /> ");
        $('#infoUsuario').append("Fechas de examenes: <br />");           
        $.each(respuestaServer.examenLoges, function(posicion, objetoJSON){
            $('#infoUsuario').append(objetoJSON.tiempo +"<br />");
        });
    }else{
        alert("Error en login = " + status );
    }
}
