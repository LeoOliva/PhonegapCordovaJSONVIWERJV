/* 
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */


/**
 * El plugin “Network Connection” nos permite obtener 
 * información acerca del tipo de conexión de red o celular 
 * que se tenga activada en el dispositivo;  a través del 
 * objeto navigator.connection.
 * 
 * 
 * La propiedad type del objeto navigator.connection 
 * comprueba la conexión y el tipo de red activa. 
 * Los valores posibles pueden ser los que están definidos 
 * en las constantes del objeto Connection
 * 
 */

function obtnerInformacionRed(){
    var tiposConexion = {};
    tiposConexion[Connection.UNKNOW] = "Desconocida";
    tiposConexion[Connection.ETHERNET] = "Ethernet";
    tiposConexion[Connection.WIFI] = "Wifi";
    tiposConexion[Connection.CELL_2G] = "Celular 2G";
    tiposConexion[Connection.CELL_3G] = "Celular 3G";
    tiposConexion[Connection.CELL_4G] = "Celular 4G";
    tiposConexion[Connection.CELL] = "Celular generica";
    tiposConexion[Connection.NONE] = "Sin conexion";
    
    var tipoConexionRed = navigator.connection.type;
    
    $("#infoDispositivoWifi").html("Tipo de conexion: " + tiposConexion[tipoConexionRed]);

}


