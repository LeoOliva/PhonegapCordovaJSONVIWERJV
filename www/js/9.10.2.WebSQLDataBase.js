/* 
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

/**
 * 
 *  Si queremos una forma más potente para almacenar datos, 
 *  PhoneGap/Cordova también nos permite usar Web SQL. 
 *  Una base de datos SQL que es almacenada localmente y 
 *  sobrevive a cierres de la app. Cuya implementación 
 *  está basada en SQLite.
 *  
 *  Para información detallada de la especificación de Web SQL Database, consulte:
 *
 *       http://www.w3.org/TR/webdatabase/
 * 
 *  La primera acción a realizar para utilizar Web SQL Database, es crear una 
 *      base de datos o conectarse a una ya existente con el método 
 *      openDatabase, en el cual:
 *
 *   -El primer parámetro indica el nombre de la base de datos.
 *   -El segundo parámetro indica la versión de la base de datos que usaremos.
 *   -El tercer parámetro es una descripción de la base de datos.
 *   -El cuarto parámetro es el tamaño estimado que tendrá la base de datos.
 * 
 * 
 *  Después de abrir la base de datos, podemos empezar a ejecutar transacciones 
 *  sobre la base de datos, esto lo hacemos usando el método db.transaccion, 
 *  en el cual:
 *
 *   -El primer parámetro indica la función callback que tendrá la o las 
 *      sentencias SQL que se incluirán en la transacción.
 *   -El segundo parámetro indica la función callback que se ejecutará 
 *      en caso de error en la transacción.
 *   -El tercer parámetro indica la función callback que se ejecutará 
 *      cuando la transacción se haya terminado.
 * 
 *  Para realizar alguna sentencia SQL la podemos hacer dentro de la función 
 *      callback de la transacción actual (recibida con el parámetro tx) 
 *      con el método executeSql en el cual:
 *
 *   -El primer parámetro especifica la sentencia SQL que se va a realizar.
 *   -El segundo parámetro especifica el arreglo de valores, si es que 
 *      utilizamos consultas parametrizadas, es decir, consultas que incluyen 
 *      valores no estáticos en la sentencia y que se representan con el 
 *      carácter ‘?’, útiles para evitar problemas de SQL-Injection.
 *   -El tercer parámetro indica la función callback al que se le mandarán 
 *      los resultados de la sentencia SQL que se ejecutó.
 *   -El cuarto parámetro indica la función callback que se ejecutará en 
 *      caso de error en la transacción.
 *
 * En la función procesaResultado la cual recibe la transacción actual (tx) 
 *  y un SQLResultSet o paquete de resultados que contiene los resultados 
 *  esperados, en esta función:
 *
 *   -La podemos recorrer usando la propiedad rows.
 *   -Para acceder a algún registro se debe usar la función rows.item(posicion).
 * 
 * En cada transacción nueva que ejecutemos o cada que ejecutemos el método 
 * db.transaction podemos hacer cuantas sentencias SQL, 
 * o métodos db.executeSql, se deseen, conforme al concepto propio 
 * del término transacción. 
 * 
 */

var db;

$(document).on("pagecontainershow",function(event,ui){
    var paginaAMostrar = ui.toPage.prop('id');
    if(paginaAMostrar == 'webSQL'){
        abreBD();
        
        consultarDatoGuardados();
    }
});

function abreBD(){
    console.log("BD abierta");
 db = openDatabase('MyDB2','1.0','DB de Notas',5 * 1024 * 1024);   
}

function consultarDatoGuardados(){
    console.log("db.transaction");
    db.transaction(consultaDB,errorTransaction,terminoTransaction);
}

function consultaDB(tx){
    console.log("Ejecutando select");
        tx.executeSql('SELECT id, nota FROM notas WHERE id > ?', [0], procesaResultado, errorTransaction);
}

function errorTransaction(tx,err){
    alert("Error en SQL: " + err.message);
}

function terminoTransaction(){
    alert("Transacción terminada");
}

function procesaResultado(tx,resultados){
    console.log('procesaResultado');
    $("#listWebSQL").html('');
    for (var i = 0; i < resultados.rows.length; i++) {
            if(resultados.rows.item(i).nota)
                       $("#listWebSQL").append("<li>" + resultados.rows.item(i).nota +"</li>");
        }
        $("#listWebSQL").listview("refresh");
       
       /*
           var misNotas = document.getElementById('misNotas');
    misNotas.innerHTML = '';
    misNotas.innerHTML = 'Consultando...<br/>';
    for (var i = 0; i < resultados.rows.length; i++) {
        var elemento = resultados.rows.item(i);
        alert(elemento.id);
        misNotas.innerHTML = misNotas.innerHTML + elemento.nota + '<br />';
    }
    */
}



$("#btnWebSQL").click(function(){
    console.log($("#inputWebSQL").val());
    if($("#inputWebSQL").val()){
        notificationNative("Se ha agregado la nota","Éxito",'OK');
        console.log('Se ha agregado la nota');
        db.transaction(insertaEnDB, errorTransaction, terminoTransaction);	
          
        consultarDatoGuardados();
        //$("#inputWebSQL").val('');
    }else{
        notificationNative("El campo esta vacío",'Error','ok');
        console.log('No se ha agregado la nota');
    }
    
    
});
function insertaEnDB(tx) {
    var nuevaNota = $('#inputWebSQL').val();
    tx.executeSql('CREATE TABLE IF NOT EXISTS notas(id INTEGER PRIMARY KEY, nota VARCHAR)');
    var insertar = 'INSERT INTO notas (nota) VALUES ("'+nuevaNota+'")';
    console.log(insertar);
    tx.executeSql(insertar);
}