/* 
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
/**
 * 
 * 
 * 
 * 
 * 
 * 
 * 
 */

var mapaGoogleMaps;


$(document).on("pagecontainershow",function(event,ui){
    if(ui.toPage.prop('id') == 'googleMaps'){
        buscaPosicion();
    }
});



//Posicion podría obtenese dese 9.11.1.golocalizacion.js, pero
// preferi copiar el funcionamiento por que se refrescaba
function presentaGoogleMapsEnUbicacion(posicion){
    var mapa = document.getElementById('divGoogleMaps');
    var latitude = posicion.coords.latitude;
    var longitude = posicion.coords.longitude;
    
    var latitudeAndLongitude = new google.maps.LatLng(latitude,longitude);
    
    var mapOptions = {
      center : latitudeAndLongitude,
      zoom : 16,
      mapTypeId : google.maps.MapTypeId.ROADMAP
    };
    
    mapaGoogleMaps = new google.maps.Map(mapa,mapOptions);
    
    agregarMarcadorEnMapa(mapaGoogleMaps,latitudeAndLongitude);
    
}

function agregarMarcadorEnMapa(mapaGoogleMaps,latitudeAndLongitude){
    var marker = new google.maps.Marker({
        position : latitudeAndLongitude,
        map : mapaGoogleMaps,
        title : 'Mi ubicacion'
    });
}
